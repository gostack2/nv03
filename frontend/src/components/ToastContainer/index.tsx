import React from 'react'

import { FiAlertCircle, FiXCircle, FiInfo, FiCheckCircle } from 'react-icons/fi';
import { ToastMessages, useToast } from '../../hooks/toast';

import { Container, Toast } from './styles'

interface ToastContainerProps {
  messages: ToastMessages[];
}

const icons = {
  info: <FiInfo size={32} />,
  success: <FiCheckCircle size={32} />,
  error: <FiAlertCircle size={32} />

}

const ToastContainer: React.FC<ToastContainerProps> = ({ messages }) => {
  const { removeToast } = useToast()

  return (
    <Container>
      {messages.map(({id, type, title, description }) => (
        <Toast
          key={id}
          type={type}
          hasDescription={!!description}
        >
        {icons[type || 'info']}

        <div>
          <strong>{title}</strong>
          {description && <p>{description}</p>}
        </div>

        <button type="button" onClick={() => removeToast(id)}>
          <FiXCircle size={16} />
        </button>
        </Toast>
      ))}

    </Container>
  )
}

export default ToastContainer;
