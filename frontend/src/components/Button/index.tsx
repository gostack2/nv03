import React, { ButtonHTMLAttributes } from 'react';

import { Container } from './styles';

type ButtonInterface = ButtonHTMLAttributes<HTMLButtonElement>;

const Button: React.FC<ButtonInterface> = ({ children, ...props }) => {
  return (
    <Container type="button" {...props}>
      {children}
    </Container>
  );
};

export default Button;
