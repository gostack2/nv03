import { shade } from 'polished';
import styled from 'styled-components';

export const Container = styled.button`
  background: ${shade(0.3, '#ff9000')};
  height: 56px;
  border-radius: 10px;
  border: none;
  padding: 0 16px;
  color: #321e38;
  width: 100%;
  font-weight: 500;
  margin-top: 16px;
  transition: background-color 0.2s;

  &:hover {
    background: #ff9000;
  }
`;
