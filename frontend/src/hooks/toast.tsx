import React, { createContext, useContext, useCallback, useState } from "react";
import { v4 as uuid } from "uuid";

import ToastContainer from "../components/ToastContainer";

export interface ToastMessages {
  id: string;
  type?: 'success' | 'error' | 'info';
  title: string;
  description?: string;
}

interface ToastContextInterface {
  addToast(message: Omit<ToastMessages, 'id'>): void;
  removeToast(id: string): void;
}

const ToastContext = createContext<ToastContextInterface>({} as ToastContextInterface);

const ToastProvider: React.FC = ({ children }) => {
  const [messages, setMessages] = useState<ToastMessages[]>([])

  const removeToast = useCallback((id: string) => {
    setMessages(messages => messages.filter(message => message.id !== id))
  },[]);

  const addToast = useCallback(({ type, title, description }: Omit<ToastMessages, 'id'>) => {
    const id = uuid();
    const toastMessage = { id, type, title, description };

    setMessages(messages => [...messages, toastMessage])
    setTimeout(() => removeToast(id), 3000)

  },[removeToast]);

  return (
    <ToastContext.Provider  value={{ addToast, removeToast }}>
      {children}
      <ToastContainer messages={messages}/>
    </ToastContext.Provider>
  )
}

function useToast() {
  const context = useContext(ToastContext)

  if (!context) {
    throw new Error('UseToast must be used within a ToastProvider')
  }

  return context;
}

export { ToastProvider, useToast }
