import React, { useRef } from 'react';
import { 
  Image,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform 
} from 'react-native';
import CreateAccountIcon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';

import { Form } from '@unform/mobile';
import { FormHandles } from '@unform/core';

import Input from '../../components/Input';
import Button from '../../components/Button';

import logoImg from '../../assets/logo.png';

import { 
  Container,
  Title,
  CreateAccountButton,
  CreateAccountButtonText,
} from './styles';


const SignUp: React.FC = () => {
  const navigation = useNavigation();
  const formRef = useRef<FormHandles>(null);

  return(
    <>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS == 'ios' ? 'padding' : undefined}
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flex: 1 }}
        >

          <Container>
            <Image source={logoImg} />

            <View>
              <Title>Crie sua conta</Title>
            </View>
            
            <Form ref={formRef} onSubmit={(data) => { console.log(data) }}>

              <Input name="name" icon="user" placeholder="Nome" />
              <Input name="email" icon="mail" placeholder="E-mail" />
              <Input name="password" icon="lock" placeholder="Senha" />

              <Button onPress={() => { formRef.current?.submitForm() }}>Cadastrar</Button>
            </Form>

          </Container>

        </ScrollView>
      </KeyboardAvoidingView>

      <CreateAccountButton onPress={() => navigation.goBack()}>
        <CreateAccountIcon name="arrow-left" size={20} color="#fff" />
        <CreateAccountButtonText>Voltar para o logon</CreateAccountButtonText>
      </CreateAccountButton>
    </>
  );
}

export default SignUp;