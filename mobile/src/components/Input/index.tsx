import React, { useEffect, useRef } from 'react';
import { TextInputProps } from 'react-native';
import { useField } from '@unform/core';

import { Container, TextInput, InputIcon } from './styles';

interface InputProps extends TextInputProps {
  name: string;
  icon: string;
}

interface inputValueRefInterface {
  value: string;
}

const Input: React.FC<InputProps> = ({ name, icon, ...rest}) => {
  const inputElementRef = useRef<any>(null);
  const { registerField, defaultValue = '', fieldName, error } = useField(name);
  const inputValueRef = useRef<inputValueRefInterface>({ value: defaultValue });

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputValueRef.current,
      path: 'value',
      setValue(ref: any, value: string) {
        inputValueRef.current.value = value;
        inputElementRef.current.setNativeProps({ text: value });
      },
      clearValue() {
        inputValueRef.current.value = '';
        inputElementRef.current.clear();
      },
    })
  }, [fieldName, registerField])

  return (
    <Container>
      <InputIcon name={icon} size={20} color="#666360" />
      <TextInput
        ref={inputElementRef}
        keyboardAppearance="dark"
        placeholderTextColor="#666360"
        defaultValue={defaultValue}
        onChangeText={ value => inputValueRef.current.value = value }
        {...rest}
      ></TextInput>
    </Container>
  );
}

export default Input;
